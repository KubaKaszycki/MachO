/* dylib.h - dynamic library support
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_DYLIB_H
#define _MACHO_DYLIB_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>

typedef struct macho_dylib
{
  macho_uint32 name;
  macho_uint32 timestamp;
  macho_uint32 curr_ver;
  macho_uint32 compat_ver;
} macho_dylib;

#endif
