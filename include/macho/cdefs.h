/* cdefs.h - definitions helping with C code and C++ compatibility
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_CDEFS_H
#define _MACHO_CDEFS_H

#ifdef __cplusplus
# define __MACHO_BEGIN_CDECL extern "C" {
# define __MACHO_END_CDECL }
#else
# define __MACHO_BEGIN_CDECL
# define __MACHO_END_CDECL
#endif

#endif
