/* cputype.h - CPU type support header
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_CPUTYPE_H
#define _MACHO_CPUTYPE_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>

typedef macho_uint32 macho_cputype;
typedef macho_uint32 macho_cpusubtype;

#define MACHO_CPUTYPE_64BIT 0x01000000

#ifdef MACHO_APPLE_ABI
# include <macho/cputype-apple.h>
# include <macho/cpusubtype-apple.h>
#else
# include <macho/cputype-gnu.h>
#endif

__MACHO_BEGIN_CDECL

const char *macho_cputype_to_string_apple (macho_cputype);
macho_cputype macho_cputype_from_string_apple (const char *);
const char *macho_cpusubtype_to_string_apple (macho_cputype, macho_cpusubtype);
macho_cpusubtype macho_cpusubtype_from_string_apple (macho_cputype,
                                                     const char *);

const char *macho_cputype_to_string_gnu (macho_cputype);
macho_cputype macho_cputype_from_string_gnu (const char *);
const char *macho_cpusubtype_to_string_gnu (macho_cputype, macho_cpusubtype);
macho_cpusubtype macho_cpusubtype_from_string_gnu (macho_cputype,
                                                   const char *);

__MACHO_END_CDECL

#ifdef MACHO_APPLE_ABI
# define macho_cputype_to_string macho_cputype_to_string_apple
# define macho_cputype_from_string macho_cputype_from_string_apple
#else
# define macho_cputype_to_string macho_cputype_to_string_gnu
# define macho_cputype_from_string macho_cputype_from_string_gnu
#endif

#endif
