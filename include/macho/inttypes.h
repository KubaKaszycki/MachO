/* inttypes.h - fixed width integer types
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_INTTYPES_H
#define _MACHO_INTTYPES_H

#include <stdint.h>

/* Edit this file on incompatible hosts */

typedef int8_t macho_int8;
typedef int16_t macho_int16;
typedef int32_t macho_int32;
typedef int64_t macho_int64;

typedef uint8_t macho_uint8;
typedef uint16_t macho_uint16;
typedef uint32_t macho_uint32;
typedef uint64_t macho_uint64;

#endif
