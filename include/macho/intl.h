#ifndef _MACHO_INTL_H
#define _MACHO_INTL_H

#include <macho/cdefs.h>

__MACHO_BEGIN_CDECL

void macho_intl_init (void);

__MACHO_END_CDECL

#endif
