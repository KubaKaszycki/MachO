/* stream.h - simple stream implementation
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_STREAM_H
#define _MACHO_STREAM_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

typedef struct macho_stream
{
  FILE *file;
  off_t base_seek;
  bool swap;
} macho_stream;

typedef enum macho_stream_seekdir
{
  MACHO_STREAM_SEEK_BEGIN = 0,
  MACHO_STREAM_SEEK_CURRENT = 1,
  /*
   * In this case, there is no seeking from the end because that would break
   * the rule of operating in files embedded in for example tar archives.
   */
} macho_stream_seekdir;

__MACHO_BEGIN_CDECL

macho_stream *macho_stream_open (const char *name, const char *mode);
macho_stream *macho_stream_open_offset (const char *name, const char *mode, off_t offset);
int macho_stream_close (macho_stream *);

int macho_stream_read (macho_stream *, size_t, void *);
int macho_stream_write (macho_stream *, size_t, const void *);

int macho_stream_read8 (macho_stream *, macho_uint8 *);
int macho_stream_read16 (macho_stream *, macho_uint16 *);
int macho_stream_read32 (macho_stream *, macho_uint32 *);
int macho_stream_read64 (macho_stream *, macho_uint64 *);

int macho_stream_write8 (macho_stream *, macho_uint8);
int macho_stream_write16 (macho_stream *, macho_uint16);
int macho_stream_write32 (macho_stream *, macho_uint32);
int macho_stream_write64 (macho_stream *, macho_uint64);

int macho_stream_seek (macho_stream *, macho_stream_seekdir, off_t);
off_t macho_stream_tell (macho_stream *);

__MACHO_END_CDECL

#endif
