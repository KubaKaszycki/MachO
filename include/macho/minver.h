#ifndef _MACHO_MINVER_H
#define _MACHO_MINVER_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>

typedef struct macho_minver
{
  macho_uint8 major;
  macho_uint8 minor;
  macho_uint8 release;
} macho_minver;

__MACHO_BEGIN_CDECL

char *macho_minver_to_string (macho_minver);

macho_minver macho_minver_extract (macho_uint32);
macho_uint32 macho_minver_store (macho_minver);

__MACHO_END_CDECL

#endif
