/* header.h - Mach-O header structures
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_HEADER_H
#define _MACHO_HEADER_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>
#include <macho/cputype.h>
#include <macho/stream.h>

#define MACHO_HEADER_MAGIC_32 0xFEEDFACE
#define MACHO_HEADER_CIGAM_32 0xCEFAEDFE
#define MACHO_HEADER_MAGIC_64 0xFEEDFACF
#define MACHO_HEADER_CIGAM_64 0xCFFAEDFE
#define MACHO_HEADER_MAGIC_FAT 0xCAFEBABE
#define MACHO_HEADER_CIGAM_FAT 0xBEBAFECA

typedef struct macho_header
{
  macho_uint32 magic;
  macho_cputype cputype;
  macho_cpusubtype cpusubtype;
  macho_uint32 filetype;
  macho_uint32 ncmds;
  macho_uint32 sizeofcmds;
  macho_uint32 flags;
} macho_header;

typedef struct macho_header64
{
  macho_uint32 magic;
  macho_cputype cputype;
  macho_cpusubtype cpusubtype;
  macho_uint32 filetype;
  macho_uint32 ncmds;
  macho_uint32 sizeofcmds;
  macho_uint32 flags;
  macho_uint32 reserved;
} macho_header64;

typedef struct macho_header_fat_arch
{
  macho_cputype cputype;
  macho_cpusubtype cpusubtype;
  macho_uint32 offset;
  macho_uint32 size;
  macho_uint32 align;
} macho_header_fat_arch;

typedef struct macho_header_fat
{
  macho_uint32 magic;
  macho_uint32 num;
  macho_header_fat_arch archs[];
} macho_header_fat;

__MACHO_BEGIN_CDECL

int macho_stream_read_header (macho_stream *stream, macho_header *header);
int macho_stream_read_header64 (macho_stream *stream, macho_header64 *header);
int macho_stream_read_header_fat (macho_stream *stream, macho_header_fat *header);
int macho_stream_read_header_fat_arch (macho_stream *stream, macho_header_fat_arch *arch);

int macho_stream_write_header (macho_stream *stream, const macho_header *header);
int macho_stream_write_header64 (macho_stream *stream, const macho_header64 *header);
int macho_stream_write_header_fat (macho_stream *stream, const macho_header_fat *header);
int macho_stream_write_header_fat_arch (macho_stream *stream, const macho_header_fat_arch *arch);

__MACHO_END_CDECL

#endif
