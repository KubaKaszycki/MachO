/* dump_lcmds.c - dumps Mach-O load commands
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <errno.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <inttypes.h>
#include <macho/loadcmd.h>
#include <macho/header.h>
#include <macho/minver.h>
#include <macho/stream.h>
#include <stdio.h>
#include <string.h>

#define FAKE_(x) x

static char fmt_uuid_buf[37];

static char *
fmt_uuid (guchar buf[16])
{
  snprintf (fmt_uuid_buf, 37, "%02hhx%02hhx%02hhx%02hhx-%02hhx%02hhx-%02hhx"
                              "%02hhx-%02hhx%02hhx-%02hhx%02hhx%02hhx%02hhx"
                              "%02hhx%02hhx", buf[0], buf[1], buf[2], buf[3],
                              buf[4], buf[5], buf[6], buf[7], buf[8], buf[9],
                              buf[10], buf[11], buf[12], buf[13], buf[14],
                              buf[15]);
  return fmt_uuid_buf;
}

static gchar *filename = NULL;

static void
dump_one (const gchar *prefix, macho_lcmd *lcmd)
{
  g_print (_ ("%s\tType:                         %s (%#x)\n"
              "%s\tSize:                         0x%08x\n"), prefix,
           macho_lcmd_type_to_string (lcmd->cmd),
           lcmd->cmd, prefix, lcmd->cmdsize);
  macho_uint32 i;
  size_t remainstoread = lcmd->cmdsize - 8;
  switch (lcmd->cmd ^ 0x80000000)
    {
    case MACHO_LCMD_SEGMENT:;
      macho_lcmd_segment *seg = (macho_lcmd_segment *) lcmd;
      g_print (_ ("%s\tName:                         %s\n"
                  "%s\tVirtual address:              0x%08" PRIx32 "\n"
                  "%s\tVirtual size:                 0x%08" PRIx32 "\n"
                  "%s\tFile address:                 0x%08" PRIx32 "\n"
                  "%s\tFile size:                    0x%08" PRIx32 "\n"
                  "%s\tMaximal protection:           0x%08" PRIx32 "\n"
                  "%s\tInitial protection:           0x%08" PRIx32 "\n"
                  "%s\tNumber of sections:           0x%08" PRIx32 "\n"
                  "%s\tFlags:                        0x%08" PRIx32 "\n"),
               prefix, seg->segname,
               prefix, seg->vmaddr,
               prefix, seg->vmsize,
               prefix, seg->fileoff,
               prefix, seg->filesize,
               prefix, seg->maxprot,
               prefix, seg->initprot,
               prefix, seg->nsects,
               prefix, seg->flags);
      remainstoread -= 48;
      break;
    case MACHO_LCMD_SYMTAB:;
      macho_lcmd_symtab *st = (macho_lcmd_symtab *) lcmd;
      g_print (_ ("%s\tSymbol offset:                0x%08" PRIx32 "\n"
                  "%s\tNumber of symbols:            0x%08" PRIx32 "\n"
                  "%s\tString table offset:          0x%08" PRIx32 "\n"
                  "%s\tString table size:            0x%08" PRIx32 "\n"),
               prefix, st->symoff,
               prefix, st->nsyms,
               prefix, st->stroff,
               prefix, st->strsize);
      remainstoread -= 16;
      break;
    case MACHO_LCMD_SYMSEG:;
      macho_lcmd_symseg *ss = (macho_lcmd_symseg *) lcmd;
      g_print (_ ("%s\tOffset:                       0x%08" PRIx32 "\n"
                  "%s\tSize:                         0x%08" PRIx32 "\n"),
               prefix, ss->offset,
               prefix, ss->size);
      remainstoread -= 8;
      break;
    case MACHO_LCMD_THREAD:
    case MACHO_LCMD_UNIXTHREAD:;
      macho_lcmd_thread *th = (macho_lcmd_thread *) lcmd;
      g_print (_ ("%s\tState flavor:                 0x%08" PRIx32 "\n"
                  "%s\tNumber of longs in state:     0x%08" PRIx32 "\n"),
               prefix, th->flavor,
               prefix, th->count);
      remainstoread -= 8;
      g_print (_ ("%s\tState:\n"), prefix);
      i = 0;
      while (i < th->count)
        {
          g_print (_ ("%s\t\t0x%08" PRIx32 "\n"), prefix, th->state[i]);
          remainstoread -= 4;
          i++;
        }
      break;
    case MACHO_LCMD_LOADFVMLIB:
    case MACHO_LCMD_IDFVMLIB:;
      macho_lcmd_fvmlib *fv = (macho_lcmd_fvmlib *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tMinor version:                0x%08" PRIx32 "\n"
                  "%s\tHeader address:               0x%08" PRIx32 "\n"),
               prefix, fv->fvmlib.name,
               prefix, fv->fvmlib.minor_version,
               prefix, fv->fvmlib.header_addr);
      remainstoread -= 12;
      break;
    case MACHO_LCMD_DYSYMTAB:;
      macho_lcmd_dysymtab *ds = (macho_lcmd_dysymtab *) lcmd;
      g_print (_ ("%s\tLocal symbols: index:         0x%08" PRIx32 "\n"
                  "%s\tLocal symbols: number:        0x%08" PRIx32 "\n"
                  "%s\tExternal symbols: index:      0x%08" PRIx32 "\n"
                  "%s\tExternal symbols: number:     0x%08" PRIx32 "\n"
                  "%s\tUndefined symbols: index:     0x%08" PRIx32 "\n"
                  "%s\tUndefined symbols: number:    0x%08" PRIx32 "\n"
                  "%s\tTable Of Contents: offset:    0x%08" PRIx32 "\n"
                  "%s\tTable Of Contents: number:    0x%08" PRIx32 "\n"
                  "%s\tModule table: offset:         0x%08" PRIx32 "\n"
                  "%s\tModule table: number:         0x%08" PRIx32 "\n"
                  "%s\tReference table: offset:      0x%08" PRIx32 "\n"
                  "%s\tReference table: number:      0x%08" PRIx32 "\n"
                  "%s\tIndirect symbols: offset:     0x%08" PRIx32 "\n"
                  "%s\tIndirect symbols: number:     0x%08" PRIx32 "\n"
                  "%s\tExternal relocation: offset:  0x%08" PRIx32 "\n"
                  "%s\tExternal relocation: number:  0x%08" PRIx32 "\n"
                  "%s\tLocal relocation; offset:     0x%08" PRIx32 "\n"
                  "%s\tLocal relocation: number:     0x%08" PRIx32 "\n"),
               prefix, ds->ilocalsym,
               prefix, ds->nlocalsym,
               prefix, ds->iextdefsym,
               prefix, ds->nextdefsym,
               prefix, ds->iundefsym,
               prefix, ds->nundefsym,
               prefix, ds->tocoff,
               prefix, ds->ntoc,
               prefix, ds->modtaboff,
               prefix, ds->nmodtab,
               prefix, ds->extrefsymoff,
               prefix, ds->nextrefsyms,
               prefix, ds->indirectsymoff,
               prefix, ds->nindirectsyms,
               prefix, ds->extreloff,
               prefix, ds->nextrel,
               prefix, ds->locreloff,
               prefix, ds->nlocrel);
      remainstoread -= 72;
      break;
    case MACHO_LCMD_LOAD_DYLIB:
    case MACHO_LCMD_ID_DYLIB:
    case MACHO_LCMD_LOAD_WEAK_DYLIB:
    case MACHO_LCMD_REEXPORT_DYLIB:
    case MACHO_LCMD_LAZY_LOAD_DYLIB:
    case MACHO_LCMD_LOAD_UPWARD_DYLIB:;
      macho_lcmd_dylib *dl = (macho_lcmd_dylib *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tTimestamp:                    0x%08" PRIx32 "\n"
                  "%s\tCurrent version:              0x%08" PRIx32 "\n"
                  "%s\tCompatibility version:        0x%08" PRIx32 "\n"
                  "%s\tName:                         %s\n"),
               prefix, dl->dylib.name,
               prefix, dl->dylib.timestamp,
               prefix, dl->dylib.curr_ver,
               prefix, dl->dylib.compat_ver,
               prefix, ((char *) lcmd) + dl->dylib.name);
      remainstoread -= 16;
      break;
    case MACHO_LCMD_LOAD_DYLINKER:
    case MACHO_LCMD_ID_DYLINKER:
    case MACHO_LCMD_DYLD_ENVIRONMENT:;
      macho_lcmd_dylinker *ld = (macho_lcmd_dylinker *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tName:                         %s\n"),
               prefix, ld->name,
               prefix, ((char *) lcmd) + ld->name);
      remainstoread -= 4;
      break;
    case MACHO_LCMD_PREBOUND_DYLIB:;
      macho_lcmd_prebound_dylib *pd = (macho_lcmd_prebound_dylib *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tNumber of modules:            0x%08" PRIx32 "\n"
                  "%s\tLinked modules:               0x%08" PRIx32 "\n"
                  "%s\tName:                         %s\n"),
               prefix, pd->name,
               prefix, pd->nmodules,
               prefix, pd->linked_modules,
               prefix, ((char *) lcmd) + pd->name);
      remainstoread -= 12;
      break;
    case MACHO_LCMD_ROUTINES:;
      macho_lcmd_routines *rt = (macho_lcmd_routines *) lcmd;
      g_print (_ ("%s\tInit address:                 0x%08" PRIx32 "\n"
                  "%s\tInit module:                  0x%08" PRIx32 "\n"
                  "%s\tReserved fields:              [ 0x%08" PRIx32 ", 0x%08"
                  PRIx32 ", 0x%08" PRIx32 ", 0x%08" PRIx32 ", 0x%08" PRIx32
                  ", 0x%08" PRIx32 " ]\n"),
               prefix, rt->init_address,
               prefix, rt->init_module,
               prefix, rt->reserved[0], rt->reserved[1], rt->reserved[2],
               rt->reserved[3], rt->reserved[4], rt->reserved[5]);
      remainstoread -= 32;
      break;
    case MACHO_LCMD_SUB_FRAMEWORK:;
      macho_lcmd_sub_framework *sf = (macho_lcmd_sub_framework *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tName:                         %s\n"),
               prefix, sf->umbrella,
               prefix, ((char *) lcmd) + sf->umbrella);
      break;
    case MACHO_LCMD_SUB_UMBRELLA:;
      macho_lcmd_sub_umbrella *su = (macho_lcmd_sub_umbrella *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tName:                         %s\n"),
               prefix, su->sub_umbrella,
               prefix, ((char *) lcmd) + su->sub_umbrella);
      break;
    case MACHO_LCMD_SUB_CLIENT:;
      macho_lcmd_sub_client *sc = (macho_lcmd_sub_client *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tName:                         %s\n"),
               prefix, sc->client,
               prefix, ((char *) lcmd) + sc->client);
      break;
    case MACHO_LCMD_SUB_LIBRARY:;
      macho_lcmd_sub_library *sl = (macho_lcmd_sub_library *) lcmd;
      g_print (_ ("%s\tName offset:                  0x%08" PRIx32 "\n"
                  "%s\tName:                         %s\n"),
               prefix, sl->sub_library,
               prefix, ((char *) lcmd) + sl->sub_library);
      break;
    case MACHO_LCMD_TWOLEVEL_HINTS:;
      macho_lcmd_twolevel_hints *tl = (macho_lcmd_twolevel_hints *) lcmd;
      g_print (_ ("%s\tOffset:                       0x%08" PRIx32 "\n"
                  "%s\tNumber of hints:              0x%08" PRIx32 "\n"),
               prefix, tl->offset,
               prefix, tl->nhints);
      break;
    case MACHO_LCMD_PREBIND_CHECKSUM:;
      macho_lcmd_prebind_checksum *pc = (macho_lcmd_prebind_checksum *) lcmd;
      g_print (_ ("%s\tChecksum:                     0x%08" PRIx32 "\n"),
               prefix, pc->checksum);
      break;
    case MACHO_LCMD_SEGMENT_64:;
      macho_lcmd_segment64 *seg64 = (macho_lcmd_segment64 *) lcmd;
      g_print (_ ("%s\tName:                         %s\n"
                  "%s\tVirtual address:              0x%016" PRIx64 "\n"
                  "%s\tVirtual size:                 0x%016" PRIx64 "\n"
                  "%s\tFile address:                 0x%016" PRIx64 "\n"
                  "%s\tFile size:                    0x%016" PRIx64 "\n"
                  "%s\tMaximal protection:           0x%08" PRIx32 "\n"
                  "%s\tInitial protection:           0x%08" PRIx32 "\n"
                  "%s\tNumber of sections:           0x%08" PRIx32 "\n"
                  "%s\tFlags:                        0x%08" PRIx32 "\n"),
               prefix, seg64->segname,
               prefix, seg64->vmaddr,
               prefix, seg64->vmsize,
               prefix, seg64->fileoff,
               prefix, seg64->filesize,
               prefix, seg64->maxprot,
               prefix, seg64->initprot,
               prefix, seg64->nsects,
               prefix, seg64->flags);
      break;
    case MACHO_LCMD_ROUTINES_64:;
      macho_lcmd_routines64 *rt64 = (macho_lcmd_routines64 *) lcmd;
      g_print (_ ("%s\tInit address:                 0x%08" PRIx64 "\n"
                  "%s\tInit module:                  0x%08" PRIx64 "\n"
                  "%s\tReserved fields:              [ 0x%08" PRIx64 ", 0x%08"
                  PRIx64 ", 0x%08" PRIx64 ", 0x%08" PRIx64 ", 0x%08" PRIx64
                  ", 0x%08" PRIx64 " ]\n"),
               prefix, rt64->init_address,
               prefix, rt64->init_module,
               prefix, rt64->reserved[0], rt64->reserved[1], rt64->reserved[2],
               rt64->reserved[3], rt64->reserved[4], rt64->reserved[5]);
      remainstoread -= 64;
      break;
    case MACHO_LCMD_UUID:;
      macho_lcmd_uuid *uu = (macho_lcmd_uuid *) lcmd;
      g_print (_ ("%s\tUUID:                         %s\n"),
               prefix, fmt_uuid (uu->uuid));
      break;
    case MACHO_LCMD_RPATH:;
      macho_lcmd_rpath *rp = (macho_lcmd_rpath *) lcmd;
      g_print (_ ("%s\tRuntime path offset:          0x%08" PRIx32 "\n"
                  "%s\tRuntime path:                 %s\n"),
               prefix, rp->path,
               prefix, ((char *) lcmd) + rp->path);
      break;
    case MACHO_LCMD_CODE_SIGNATURE:
    case MACHO_LCMD_SEGMENT_SPLIT_INFO:
    case MACHO_LCMD_FUNCTION_STARTS:
    case MACHO_LCMD_DATA_IN_CODE:
    case MACHO_LCMD_DYLIB_CODE_SIGN_DRS:
    case MACHO_LCMD_LINKER_OPTIMIZATION_HINT:;
      macho_lcmd_linkedit_data *le = (macho_lcmd_linkedit_data *) lcmd;
      g_print (_ ("%s\tData offset                   0x%08" PRIx32 "\n"
                  "%s\tData size:                    0x%08" PRIx32 "\n"),
               prefix, le->dataoff,
               prefix, le->datasize);
      break;
    case MACHO_LCMD_ENCRYPTION_INFO:;
      macho_lcmd_encryption_info *ei = (macho_lcmd_encryption_info *) lcmd;
      /* TODO: Encryption ID decoding */
      g_print (_ ("%s\tEncrypted data offset:        0x%08" PRIx32 "\n"
                  "%s\tEncrypted data length:        0x%08" PRIx32 "\n"
                  "%s\tEncryption system:            0x%08" PRIx32 "\n"),
               prefix, ei->cryptoff,
               prefix, ei->cryptsize,
               prefix, ei->cryptid);
      break;
    case MACHO_LCMD_DYLD_INFO:;
      macho_lcmd_dyld_info *di = (macho_lcmd_dyld_info *) lcmd;
      g_print (_ ("%s\tRebase information offset:    0x%08" PRIx32 "\n"
                  "%s\tRebase information size:      0x%08" PRIx32 "\n"
                  "%s\tBinding information offset:   0x%08" PRIx32 "\n"
                  "%s\tBinding information size:     0x%08" PRIx32 "\n"
                  "%s\tWeak binding info offset:     0x%08" PRIx32 "\n"
                  "%s\tWeak binding info size:       0x%08" PRIx32 "\n"
                  "%s\tLazy binding info offset:     0x%08" PRIx32 "\n"
                  "%s\tLazy binding info size:       0x%08" PRIx32 "\n"
                  "%s\tExported symbol info offset:  0x%08" PRIx32 "\n"
                  "%s\tExported symbol info size:    0x%08" PRIx32 "\n"),
               prefix, di->rebase_off,
               prefix, di->rebase_size,
               prefix, di->bind_off,
               prefix, di->bind_size,
               prefix, di->weak_bind_off,
               prefix, di->weak_bind_size,
               prefix, di->lazy_bind_off,
               prefix, di->lazy_bind_size,
               prefix, di->export_off,
               prefix, di->export_size);
      break;
    case MACHO_LCMD_VERSION_MIN_MACOSX:
    case MACHO_LCMD_VERSION_MIN_IPHONEOS:
    case MACHO_LCMD_VERSION_MIN_WATCHOS:;
      macho_lcmd_version_min *vi = (macho_lcmd_version_min *) lcmd;
      char *version_str = macho_minver_to_string (macho_minver_extract (vi->version));
      char *sdk_str = macho_minver_to_string (macho_minver_extract (vi->sdk));
      g_print (_ ("%s\tVersion:                      %s (0x%08" PRIx32 ")\n"
                  "%s\tSDK version:                  %s (0x%08" PRIx32 ")\n"),
               prefix, version_str, vi->version,
               prefix, sdk_str, vi->sdk);
      g_free (version_str);
      g_free (sdk_str);
      break;
    case MACHO_LCMD_MAIN:;
      macho_lcmd_entry_point *ep = (macho_lcmd_entry_point *) lcmd;
      g_print (_ ("%s\tEntry offset:                 0x%016" PRIx64 "\n"
                  "%s\tStack size:                   0x%016" PRIx64 "\n"),
               prefix, ep->entryoff,
               prefix, ep->stacksize);
      break;
    case MACHO_LCMD_SOURCE_VERSION:;
      macho_lcmd_source_version *sv = (macho_lcmd_source_version *) lcmd;
      g_print (_ ("%s\tVersion:                      0x%016" PRIx64 "\n"),
               prefix, sv->version);
      break;
    case MACHO_LCMD_ENCRYPTION_INFO_64:;
      macho_lcmd_encryption_info64 *ei64 = (macho_lcmd_encryption_info64 *) lcmd;
      /* TODO: Encryption ID decoding */
      g_print (_ ("%s\tEncrypted data offset:        0x%08" PRIx32 "\n"
                  "%s\tEncrypted data length:        0x%08" PRIx32 "\n"
                  "%s\tEncryption system:            0x%08" PRIx32 "\n"),
               prefix, ei64->cryptoff,
               prefix, ei64->cryptsize,
               prefix, ei64->cryptid);
      break;
    case MACHO_LCMD_LINKER_OPTION:;
      macho_lcmd_linker_option *lo = (macho_lcmd_linker_option *) lcmd;
      g_print (_ ("%s\tCount:                        " PRIu32 "\n"),
               prefix, lo->count);
      break;
    }

  /*g_print (_ ("%s\tRemaining string:             %*s\n"),
           prefix, remainstoread,
           ((char *) lcmd) + (lcmd->cmdsize - remainstoread));*/
}

static int
do_stream_normal (const gchar *prefix, macho_stream *stream, bool is64)
{
  macho_header hdr;
  if (is64)
    {
      macho_header64 hdr64;
      if (macho_stream_read_header64 (stream, &hdr64) != 0)
        {
          fprintf (stderr, "Could not read header: %s\n", g_strerror (errno));
          return 1;
        }

      memcpy (&hdr, &hdr64, sizeof (hdr));
    }
  else
    {
      if (macho_stream_read_header (stream, &hdr) != 0)
        {
          fprintf (stderr, "Could not read header: %s\n", g_strerror (errno));
          return 1;
        }
    }

  macho_uint32 i = 0;
  
  while (i < hdr.ncmds)
    {
      g_print (_ ("%sLoad command %u:\n"), prefix, i);
      macho_lcmd *lcmd = macho_stream_read_lcmd (stream);
      if (lcmd == NULL)
        {
          fprintf (stderr, "Could not read from %s: %s\n", filename, g_strerror
                   (errno));
          return 1;
        }
      dump_one (prefix, lcmd);
      g_free (lcmd);
      i++;
    }

  return 0;
}

static int do_stream (const gchar *prefix, macho_stream *stream);

static int
do_stream_fat (const gchar *prefix, macho_stream *stream)
{
  macho_header_fat hdr;
  macho_header_fat_arch arch;

  gchar *prefix_sub = g_strjoin (prefix, "\t", NULL);
  if (macho_stream_read_header_fat (stream, &hdr) != 0)
    {
      fprintf (stderr, "Could not read header: %s\n", g_strerror (errno));
      g_free (prefix_sub);
      return 1;
    }
  g_print (_ ("Magic number:                  %0#10x\n"), hdr.magic);
  g_print (_ ("%u architectures:\n"), hdr.num);
  macho_uint32 i = 0;
  while (i < hdr.num)
    {
      g_print (_ ("Architecture %u:\n"), i);
      if (macho_stream_read_header_fat_arch (stream, &arch) != 0)
        {
          fprintf (stderr, "Could not read architecture: %s\n", g_strerror
                   (errno));
          g_free (prefix_sub);
          return 1;
        }
      g_print (_ ("%s\tOffset:                        %0#10x\n"
                  "%s\tSize:                          %0#10x\n"
                  "%s\tAlignment:                     %0#10x\n"),
               prefix, arch.offset,
               prefix, arch.size,
               prefix, arch.align);

      macho_stream *arch_stream = macho_stream_open_offset (filename, "r",
                                                            arch.offset);

      if (arch_stream == NULL)
        {
          fprintf (stderr, "Could not open sub-stream: %s\n", g_strerror
                   (errno));
          g_free (prefix_sub);
          return 1;
        }

      int i2;
      if ((i2 = do_stream (prefix_sub, arch_stream)) != 0)
        {
          macho_stream_close (arch_stream);
          return i2;
        }

      macho_stream_close (arch_stream);

      i++;
    }
  g_free (prefix_sub);

  return 0;
}

static int
do_stream (const gchar *prefix, macho_stream *stream)
{
  macho_uint32 magic;

  if (macho_stream_read32 (stream, &magic) != 0)
    {
      fprintf (stderr, "Could not read magic number: %s\n", g_strerror
               (errno));
      return 1;
    }

  if (macho_stream_seek (stream, MACHO_STREAM_SEEK_CURRENT, -4) != 0)
    {
      fprintf (stderr, "Could not seek: %s\n", g_strerror (errno));
      return 1;
    }

  switch (magic)
    {
    case MACHO_HEADER_MAGIC_32:
    case MACHO_HEADER_MAGIC_64:
      return do_stream_normal (prefix, stream, magic == MACHO_HEADER_MAGIC_64);
    case MACHO_HEADER_MAGIC_FAT:
      return do_stream_fat (prefix, stream);
    default:
      fprintf (stderr, "Unrecognized file!\n");
      return 1;
    }
}

int
dump_lcmds (gchar ** args)
{
  GError *error = NULL;
  gchar **filenames = NULL;

  static gchar *arch = NULL;

  const GOptionEntry ENTRIES[] = {
    {"arch", 'a', 0, G_OPTION_ARG_STRING, &arch,
      FAKE_ ("Dumps only the selected architecture of a fat file"), FAKE_ ("<ARCHITECTURE>")},
    {"", 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &filenames, NULL, FAKE_ ("<FILE>")},
    {NULL, 0, 0, 0, NULL, NULL, NULL}
  };

  g_set_prgname ("macho dump_lcmds");

  GOptionContext *optctx = g_option_context_new (_("- Dump information from "
                                                   "Mach-O load commands"));

  g_option_context_add_main_entries (optctx, ENTRIES, "macho");

  if (!g_option_context_parse_strv (optctx, &args, &error))
    {
      fprintf (stderr, _("Option parsing error: [%04u] %s\n"), error->code,
               error->message);
    }

  bool multifile = false;

  if (filenames == NULL || filenames[0] == NULL)
    {
      fprintf (stderr, _("Usage: macho dump_lcmds [OPTION...] <FILE>\n"
                         "Run with --help for more information\n"));
      return 2;
    }
  else if (filenames[1] != NULL)
    {
      multifile = true;
    }

  while (*filenames)
    {
      filename = *filenames;

      if (multifile)
        g_print (_ ("File %s:\n"), filename);

      macho_stream *stream = macho_stream_open (filename, "r");

      if (stream == NULL)
        {
          fprintf (stderr, "Could not open %s: %s\n", filename, g_strerror
                   (errno));
          return 1;
        }

      int ret = do_stream (multifile ? "\t" : "", stream);

      macho_stream_close (stream);
      
      if (ret != 0)
        {
          return ret;
        }

      filenames++;
    }

  return 0;
}
