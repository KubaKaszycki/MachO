/* shell.c - shell mode of MachO utility
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#ifndef HAVE_RL
#define HAVE_RL 0
#endif
#ifndef HAVE_RL_HIST
#define HAVE_RL_HIST 0
#endif
#ifndef HAVE_READLINE_H
#define HAVE_READLINE_H 0
#endif
#ifndef HAVE_READLINE_READLINE_H
#define HAVE_READLINE_READLINE_H 0
#endif
#ifndef HAVE_READLINE_HISTORY_H
#define HAVE_READLINE_HISTORY_H 0
#endif

#include <errno.h>
#include <execute.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <macho/header.h>
#include <macho/stream.h>
#if HAVE_RL && HAVE_READLINE_H
#include <readline.h>
#endif
#if HAVE_RL && HAVE_READLINE_HISTORY_H
#include <readline/history.h>
#endif
#if HAVE_RL && HAVE_RL_HIST && HAVE_READLINE_READLINE_H
#include <readline/readline.h>
#endif

#include "dirs.h"

#define FAKE_(x) x

extern char *alias;

int
shell (gchar ** args)
{
  GError *error = NULL;

  g_set_prgname ("macho shell");

  GOptionContext *optctx = g_option_context_new (_("- Start an interactive "
                                                   "MachO shell"));

  if (!g_option_context_parse_strv (optctx, &args, &error))
    {
      fprintf (stderr, _("Option parsing error: [%04u] %s\n"), error->code,
               error->message);
      return 2;
    }

#if HAVE_RL
  rl_readline_name = "macho";
#endif

  puts (_("MachO Copyright (C) 2017 Jakub Kaszycki\n"
          "This program comes with ABSOLUTELY NO WARRANTY; for details type "
          "`license'.\n"
          "This is free software, and you are welcome to redistribute it\n"
          "under certain conditions; type `license' for details."));


  char *str = NULL;

  while ((str = readline ("MachO> ")) != NULL)
    {
      gchar *cmdline = g_strjoin (" ", alias, str, NULL);
      gchar **new_args;
      gint argc_unused;
      g_shell_parse_argv (cmdline, &argc_unused, &new_args, &error);
      g_free (cmdline);

      if (strcmp (new_args[1], "exit") == 0)
        {
          g_free (str);
          g_strfreev (new_args);
          break;
        }

      int result = execute (alias, alias, new_args, false, false, false,
                            false, true, false, NULL);

      if (result != 0)
        {
          g_print (_("Subprocess returned exit code %u\n"), result);
        }

      g_strfreev (new_args);

#if HAVE_RL_HIST
      add_history (str);
#endif
      g_free (str);
    }

  return 0;
}
