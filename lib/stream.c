/* stream.c - simple stream implementation
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <errno.h>
#include <fcntl.h>
#include <macho/header.h>
#include <macho/stream.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef __GNUC__
__attribute__ ((__always_inline__))
#endif
static inline macho_uint16
SWAP16 (macho_uint16 x)
{
  macho_uint16 ret = 0;

  ret |= ((x >> 8) & 0xFF);
  ret |= ((x << 8) & 0xFF00);

  return ret;
}

#ifdef __GNUC__
__attribute__ ((__always_inline__))
#endif
static inline macho_uint32
SWAP32 (macho_uint32 x)
{
  macho_uint32 ret = 0;
  
  ret |= ((x >> 24) & 0xFF);
  ret |= ((x >> 8) & 0xFF00);
  ret |= ((x << 8) & 0xFF0000);
  ret |= ((x << 24) & 0xFF000000);
  
  return ret;
}

#ifdef __GNUC__
__attribute__ ((__always_inline__))
#endif
static inline macho_uint64
SWAP64 (macho_uint64 x)
{
  macho_uint64 ret = 0;

  ret |= ((x >> 56) & 0xFFL);
  ret |= ((x >> 40) & 0xFF00L);
  ret |= ((x >> 24) & 0xFF0000L);
  ret |= ((x >> 8) & 0xFF000000L);
  ret |= ((x << 8) & 0xFF00000000L);
  ret |= ((x << 24) & 0xFF0000000000L);
  ret |= ((x << 40) & 0xFF000000000000L);
  ret |= ((x << 56) & 0xFF00000000000000L);
  
  return ret;
}

static int
detect_magic (macho_stream * strm)
{
  /* If nothing gets written, we'll se it: */
  macho_uint32 magic = 0x12345678;
  if (macho_stream_read32 (strm, &magic) != 0)
    {
      if (feof (strm->file))
        {
          /* Assume unchanged */
          fseek (strm->file, -4, SEEK_CUR);
          return 0;
        }
      return -1;
    }
  switch (magic)
    {
    case MACHO_HEADER_MAGIC_32:
    case MACHO_HEADER_MAGIC_64:
    case MACHO_HEADER_MAGIC_FAT:
      fseek (strm->file, -4, SEEK_CUR);
      return 0;
    case MACHO_HEADER_CIGAM_32:
    case MACHO_HEADER_CIGAM_64:
    case MACHO_HEADER_CIGAM_FAT:
      fseek (strm->file, -4, SEEK_CUR);
      return 1;
    default:
      errno = EINVAL;
      return -1;
    }
}

macho_stream *
macho_stream_open (const char *file, const char *openmode)
{
  return macho_stream_open_offset (file, openmode, 0);
}

macho_stream *
macho_stream_open_offset (const char *file, const char *openmode, off_t off)
{
  macho_stream *strm = (macho_stream *) malloc (sizeof (macho_stream));
  if (strm == NULL)
    {
      return NULL;
    }

  strm->file = fopen (file, openmode);

  if (strm->file == NULL)
    {
      free (strm);
      return NULL;
    }
  strm->base_seek = off;

  fseek (strm->file, off, SEEK_SET);

  /* Temporarily... */
  strm->swap = false;

  switch (detect_magic (strm))
    {
    case -1:
      fclose (strm->file);
      free (strm);
      return NULL;
    case 0:
      break;
    case 1:
      strm->swap = true;
      break;
    }

  return strm;
}

int
macho_stream_close (macho_stream * stream)
{
  if (fclose (stream->file) != 0)
    {
      return -1;
    }
  free (stream);
  return 0;
}

int
macho_stream_read (macho_stream * stream, size_t size, void *tgtbuf)
{
  if (size == 0)
    return 0;
  return fread (tgtbuf, size, 1, stream->file) == 1 ? 0 : -1;
}

int
macho_stream_write (macho_stream * stream, size_t size, const void *srcbuf)
{
  if (size == 0)
    return 0;
  return fwrite (srcbuf, size, 1, stream->file) == 1 ? 0 : -1;
}

int
macho_stream_read8 (macho_stream * stream, macho_uint8 * out)
{
  if (macho_stream_read (stream, 1, out) != 0)
    {
      return -1;
    }
  return 0;
}

int
macho_stream_read16 (macho_stream * stream, macho_uint16 * out)
{
  if (macho_stream_read (stream, 2, out) != 0)
    {
      return -1;
    }
  if (stream->swap)
    *out = SWAP16 (*out);
  return 0;
}

int
macho_stream_read32 (macho_stream * stream, macho_uint32 * out)
{
  if (macho_stream_read (stream, 4, out) != 0)
    {
      return -1;
    }
  if (stream->swap)
    *out = SWAP32 (*out);
  return 0;
}

int
macho_stream_read64 (macho_stream * stream, macho_uint64 * out)
{
  if (macho_stream_read (stream, 8, out) != 0)
    {
      return -1;
    }

  if (stream->swap)
    *out = SWAP64 (*out);
  return 0;
}

int
macho_stream_write8 (macho_stream * stream, macho_uint8 in)
{
  if (macho_stream_write (stream, 1, &in) != 0)
    {
      return -1;
    }

  return 0;
}

int
macho_stream_write16 (macho_stream * stream, macho_uint16 in)
{
  if (stream->swap)
    in = SWAP16 (in);
  if (macho_stream_write (stream, 2, &in) != 0)
    {
      return -1;
    }

  return 0;
}

int
macho_stream_write32 (macho_stream * stream, macho_uint32 in)
{
  if (stream->swap)
    in = SWAP32 (in);
  if (macho_stream_write (stream, 4, &in) != 0)
    {
      return -1;
    }

  return 0;
}

int
macho_stream_write64 (macho_stream * stream, macho_uint64 in)
{
  if (stream->swap)
    in = SWAP64 (in);
  if (macho_stream_write (stream, 8, &in) != 0)
    {
      return -1;
    }

  return 0;
}

int
macho_stream_seek (macho_stream * stream, macho_stream_seekdir dir, off_t off)
{
  switch (dir)
    {
    case MACHO_STREAM_SEEK_BEGIN:
      if (fseek (stream->file, stream->base_seek + off, SEEK_SET) != 0)
        {
          return -1;
        }
      return 0;
    case MACHO_STREAM_SEEK_CURRENT:
      if (fseek (stream->file, off, SEEK_CUR) != 0)
        {
          return -1;
        }
      return 0;
    default:
      return -1;
    }
}

off_t
macho_stream_tell (macho_stream * stream)
{
  return ftell (stream->file);
}
