/* loadcmd.c - Load command I/O routines
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Please keep in sync with lib/loadcmd.c and data/loadcmd.defs
 */

#include <config.h>

#include <errno.h>
#include <macho/loadcmd.h>
#include <macho/stream.h>
#include <stdlib.h>
#include <string.h>

#define READ(s, p) \
  do \
    { \
      if (macho_stream_read (strm, s, p) != 0) \
        { \
          int errno_bak = errno; \
          free (ret); \
          errno = errno_bak; \
          return NULL; \
        } \
      cur_cmdsize += s; \
    } \
  while (0)
#define READ32(p) \
  do \
    { \
      if (macho_stream_read32 (strm, &p) != 0) \
        { \
          int errno_bak = errno; \
          free (ret); \
          errno = errno_bak; \
          return NULL; \
        } \
      cur_cmdsize += 4; \
    } \
  while (0)
#define READ64(p) \
  do \
    { \
      if (macho_stream_read64 (strm, &p) != 0) \
        { \
          int errno_bak = errno; \
          free (ret); \
          errno = errno_bak; \
          return NULL; \
        } \
      cur_cmdsize += 8; \
    } \
  while (0)
macho_lcmd *
macho_stream_read_lcmd (macho_stream *strm)
{
  macho_uint32 cmd, cmdsize;
  if (macho_stream_read32 (strm, &cmd) != 0)
    {
      return NULL;
    }
  if (macho_stream_read32 (strm, &cmdsize) != 0)
    {
      return NULL;
    }

  macho_lcmd *ret = (macho_lcmd *) malloc (cmdsize);
  if (ret == NULL)
    {
      return NULL;
    }
  ret->cmd = cmd;
  ret->cmdsize = cmdsize;

  macho_uint32 cur_cmdsize = 8;
  macho_uint32 i = 0;

  switch (cmd)
    {
    case MACHO_LCMD_SEGMENT:;
      macho_lcmd_segment *seg = (macho_lcmd_segment *) ret;
      READ (16, seg->segname);
      READ32 (seg->vmaddr);
      READ32 (seg->vmsize);
      READ32 (seg->fileoff);
      READ32 (seg->filesize);
      READ32 (seg->maxprot);
      READ32 (seg->initprot);
      READ32 (seg->nsects);
      READ32 (seg->flags);
      break;
    case MACHO_LCMD_SYMTAB:;
      macho_lcmd_symtab *st = (macho_lcmd_symtab *) ret;
      READ32 (st->symoff);
      READ32 (st->nsyms);
      READ32 (st->stroff);
      READ32 (st->strsize);
      break;
    case MACHO_LCMD_SYMSEG:;
      macho_lcmd_symseg *ss = (macho_lcmd_symseg *) ret;
      READ32 (ss->offset);
      READ32 (ss->size);
      break;
    case MACHO_LCMD_THREAD:
    case MACHO_LCMD_UNIXTHREAD:;
      macho_lcmd_thread *th = (macho_lcmd_thread *) ret;
      READ32 (th->flavor);
      READ32 (th->count);
      while (i < th->count)
        {
          READ32 (th->state[i]);
          i++;
        }
      break;
    case MACHO_LCMD_LOADFVMLIB:
    case MACHO_LCMD_IDFVMLIB:;
      macho_lcmd_fvmlib *fv = (macho_lcmd_fvmlib *) ret;
      READ32 (fv->fvmlib.name);
      READ32 (fv->fvmlib.minor_version);
      READ32 (fv->fvmlib.header_addr);
      break;
    case MACHO_LCMD_DYSYMTAB:;
      macho_lcmd_dysymtab *ds = (macho_lcmd_dysymtab *) ret;
      READ32 (ds->ilocalsym);
      READ32 (ds->nlocalsym);
      READ32 (ds->iextdefsym);
      READ32 (ds->nextdefsym);
      READ32 (ds->iundefsym);
      READ32 (ds->nundefsym);
      READ32 (ds->tocoff);
      READ32 (ds->ntoc);
      READ32 (ds->modtaboff);
      READ32 (ds->nmodtab);
      READ32 (ds->extrefsymoff);
      READ32 (ds->nextrefsyms);
      READ32 (ds->indirectsymoff);
      READ32 (ds->nindirectsyms);
      READ32 (ds->extreloff);
      READ32 (ds->nextrel);
      READ32 (ds->locreloff);
      READ32 (ds->nlocrel);
      break;
    case MACHO_LCMD_LOAD_DYLIB:
    case MACHO_LCMD_ID_DYLIB:
    case MACHO_LCMD_LOAD_WEAK_DYLIB:
    case MACHO_LCMD_REEXPORT_DYLIB:
    case MACHO_LCMD_LAZY_LOAD_DYLIB:
    case MACHO_LCMD_LOAD_UPWARD_DYLIB:;
      macho_lcmd_dylib *dl = (macho_lcmd_dylib *) ret;
      READ32 (dl->dylib.name);
      READ32 (dl->dylib.timestamp);
      READ32 (dl->dylib.curr_ver);
      READ32 (dl->dylib.compat_ver);
      break;
    case MACHO_LCMD_LOAD_DYLINKER:
    case MACHO_LCMD_ID_DYLINKER:
    case MACHO_LCMD_DYLD_ENVIRONMENT:;
      macho_lcmd_dylinker *ld = (macho_lcmd_dylinker *) ret;
      READ32 (ld->name);
      break;
    case MACHO_LCMD_PREBOUND_DYLIB:;
      macho_lcmd_prebound_dylib *pd = (macho_lcmd_prebound_dylib *) ret;
      READ32 (pd->name);
      READ32 (pd->nmodules);
      READ32 (pd->linked_modules);
      break;
    case MACHO_LCMD_ROUTINES:;
      macho_lcmd_routines *rt = (macho_lcmd_routines *) ret;
      READ32 (rt->init_address);
      READ32 (rt->init_module);
      i = 0;
      while (i < 6)
        {
          READ32 (rt->reserved[i]);
          i++;
        }
      break;
    case MACHO_LCMD_SUB_FRAMEWORK:;
      macho_lcmd_sub_framework *sf = (macho_lcmd_sub_framework *) ret;
      READ32 (sf->umbrella);
      break;
    case MACHO_LCMD_SUB_UMBRELLA:;
      macho_lcmd_sub_umbrella *su = (macho_lcmd_sub_umbrella *) ret;
      READ32 (su->sub_umbrella);
      break;
    case MACHO_LCMD_SUB_CLIENT:;
      macho_lcmd_sub_client *sc = (macho_lcmd_sub_client *) ret;
      READ32 (sc->client);
      break;
    case MACHO_LCMD_SUB_LIBRARY:;
      macho_lcmd_sub_library *sl = (macho_lcmd_sub_library *) ret;
      READ32 (sl->sub_library);
      break;
    case MACHO_LCMD_TWOLEVEL_HINTS:;
      macho_lcmd_twolevel_hints *tl = (macho_lcmd_twolevel_hints *) ret;
      READ32 (tl->offset);
      READ32 (tl->nhints);
      break;
    case MACHO_LCMD_PREBIND_CHECKSUM:;
      macho_lcmd_prebind_checksum *pc = (macho_lcmd_prebind_checksum *) ret;
      READ32 (pc->checksum);
      break;
    case MACHO_LCMD_SEGMENT_64:;
      macho_lcmd_segment64 *seg64 =  (macho_lcmd_segment64 *) ret;
      READ (16, seg64->segname);
      READ64 (seg64->vmaddr);
      READ64 (seg64->vmsize);
      READ64 (seg64->fileoff);
      READ64 (seg64->filesize);
      READ32 (seg64->maxprot);
      READ32 (seg64->initprot);
      READ32 (seg64->nsects);
      READ32 (seg64->flags);
      break;
    case MACHO_LCMD_ROUTINES_64:;
      macho_lcmd_routines64 *rt64 = (macho_lcmd_routines64 *) ret;
      READ64 (rt64->init_address);
      READ64 (rt64->init_module);
      i = 0;
      while (i < 6)
        {
          READ64 (rt64->reserved[i]);
          i++;
        }
      break;
    case MACHO_LCMD_UUID:;
      macho_lcmd_uuid *uu = (macho_lcmd_uuid *) ret;
      READ (16, uu->uuid);
      break;
    case MACHO_LCMD_RPATH:;
      macho_lcmd_rpath *rp = (macho_lcmd_rpath *) ret;
      READ32 (rp->path);
      break;
    case MACHO_LCMD_CODE_SIGNATURE:
    case MACHO_LCMD_SEGMENT_SPLIT_INFO:
    case MACHO_LCMD_FUNCTION_STARTS:
    case MACHO_LCMD_DATA_IN_CODE:
    case MACHO_LCMD_DYLIB_CODE_SIGN_DRS:
    case MACHO_LCMD_LINKER_OPTIMIZATION_HINT:;
      macho_lcmd_linkedit_data *le = (macho_lcmd_linkedit_data *) ret;
      READ32 (le->dataoff);
      READ32 (le->datasize);
      break;
    case MACHO_LCMD_ENCRYPTION_INFO:;
      macho_lcmd_encryption_info *ei = (macho_lcmd_encryption_info *) ret;
      READ32 (ei->cryptoff);
      READ32 (ei->cryptsize);
      READ32 (ei->cryptid);
      break;
    case MACHO_LCMD_DYLD_INFO:;
      macho_lcmd_dyld_info *di = (macho_lcmd_dyld_info *) ret;
      READ32 (di->rebase_off);
      READ32 (di->rebase_size);
      READ32 (di->bind_off);
      READ32 (di->bind_size);
      READ32 (di->weak_bind_off);
      READ32 (di->weak_bind_size);
      READ32 (di->lazy_bind_off);
      READ32 (di->lazy_bind_size);
      READ32 (di->export_off);
      READ32 (di->export_size);
      break;
    case MACHO_LCMD_VERSION_MIN_MACOSX:
    case MACHO_LCMD_VERSION_MIN_IPHONEOS:
    case MACHO_LCMD_VERSION_MIN_WATCHOS:;
      macho_lcmd_version_min *vi = (macho_lcmd_version_min *) ret;
      READ32 (vi->version);
      READ32 (vi->sdk);
      break;
    case MACHO_LCMD_MAIN:;
      macho_lcmd_entry_point *ep = (macho_lcmd_entry_point *) ret;
      READ64 (ep->entryoff);
      READ64 (ep->stacksize);
      break;
    case MACHO_LCMD_SOURCE_VERSION:;
      macho_lcmd_source_version *sv = (macho_lcmd_source_version *) ret;
      READ64 (sv->version);
      break;
    case MACHO_LCMD_ENCRYPTION_INFO_64:;
      macho_lcmd_encryption_info64 *ei64 = (macho_lcmd_encryption_info64 *) ret;
      READ32 (ei64->cryptoff);
      READ32 (ei64->cryptsize);
      READ32 (ei64->cryptid);
      READ32 (ei64->pad);
      break;
    case MACHO_LCMD_LINKER_OPTION:;
      macho_lcmd_linker_option *lo = (macho_lcmd_linker_option *) ret;
      READ32 (lo->count);
      break;
    default:
      if (macho_stream_read (strm, cmdsize - 8, ((char *) ret) + 8) != 0)
        {
          free (ret);
          return NULL;
        }
      return ret;
    }

    READ (cmdsize - cur_cmdsize, ((char *) ret + cur_cmdsize));
    return ret;
}

const char *
macho_lcmd_type_to_string (macho_uint32 ct)
{
  ct = ct & 0x7FFFFFFF;
#define LCMD(name, code) \
  if (ct == code) \
    { \
      return #name; \
    }
#include "loadcmd.inc"
#undef LCMD
  return NULL;
}

macho_uint32
macho_lcmd_type_from_string (const char *str)
{
  if (str == NULL)
    return 0;

#define LCMD(name, code) \
  if (strcmp (str, #name) == 0) \
    { \
      return code; \
    }
#include "loadcmd.inc"
#undef LCMD
  return 0; /* Zero is not occupied and most probably will never be */
}

