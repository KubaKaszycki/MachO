/* filetype.c - file type recogniser routines
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <macho/filetype.h>

#include <string.h>

const char *
macho_file_type_to_string (macho_uint32 ct)
{
#define FILE_TYPE(name, code) \
  if (ct == code) \
    { \
      return #name; \
    }
#include "filetype.inc"
#undef FILE_TYPE
  return NULL;
}

macho_uint32
macho_file_type_from_string (const char *str)
{
  if (str == NULL)
    return 0;

#define FILE_TYPE(name, code) \
  if (strcmp (str, #name) == 0) \
    { \
      return code; \
    }
#include "filetype.inc"
#undef FILE_TYPE
  return 0; /* Zero is not occupied and most probably will never be */
}
