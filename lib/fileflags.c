/* fileflags.c - handlers for the `flags' field of header
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <macho/filetype.h>

#include <glib.h>
#include <string.h>

const char *
macho_file_flag_to_string (unsigned int bit)
{
#define FILE_FLAG(name, bit2) \
  if (bit == bit2) \
    return #name;
#include "fileflags.inc"
#undef FILE_FLAG
  return NULL;
}

unsigned int
macho_file_flag_from_string (const char *str)
{
  if (str == NULL)
    return ((unsigned int) -1);
#define FILE_FLAG(name, bit) \
  if (strcmp (#name, str) == 0) \
    return bit;
#include "fileflags.inc"
#undef FILE_FLAG
  return ((unsigned int) -1);
}

char *
macho_file_flags_to_string (macho_uint32 flags)
{
  GString *str = g_string_new ("");

  unsigned int i = 0;
  while (i < 31)
    {
      if (flags & (1 << i))
        {
          const char *flag_str = macho_file_flag_to_string (i);
          if (flag_str != NULL)
            {
              g_string_append_printf (str, "%s ", flag_str);
            }
        }

      i++;
    }

  char *ret = str->str;

  g_string_free (str, FALSE);

  return ret;
}
